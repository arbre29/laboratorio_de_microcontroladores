/** @file dice_sum.c
 *  @brief Programa que permite simular el funcionamiento de un dado con un PIC12F683.
 *  @author Alexander Rojas Brenes -B86869
 *  @date 01/04/2024
 */

#include <pic14/pic12f683.h>

// Se deshabilita el Watch Dog Timer y el MCLRE en el GPIO3.
typedef unsigned int word;
word __at 0x2007 __CONFIG = (_WDT_OFF & _MCLRE_OFF);

// Se definen los prototipos de cada una de las funciones
void delay (unsigned int tiempo);

unsigned int sonCoprimos(int a, int b);

int seed_adecuada(int M, int S);

unsigned int blum_blum_shub(int p, int q, int seed);


/** @brief Función principal.
 *  @details Esta función se encarga de orquestar el resto de funciones 
 * 	programadas. En primer lugar, se define el pin GP3 como entrada, de tal 
 * 	forma que este pueda se utilizado para "lanzar el dado" (reiniciar el 
 * 	microcontrolador y producir un nuevo número aleatorio que será mostrado a
 *  través de los LEDs). Asimismo, se definen e inicializan algunas variables 
 * 	necesarias para el funcionamiento del generador de número aleatorio. 
 * 	Después, se utiliza un ciclo infinito que representa el funcionamiento del 
 * 	microcontrolador y en este se verifica si el botón fue presionado (GP3 = 0)
 *  , si es así, se genera un número aleatorio y se usa un switch para iluminar
 * 	los LEDs (modificar el valor de GPIO) según sea el caso. Finalmente, en 
 * 	cada iteración del ciclo se modifica el valor de la semilla para aumentar 
 * 	la variabilidad en la generación de número aleatorios. 
*/
void main(void)
{
	TRISIO = 0b11001000; // Se define el GP3 con entrada y el resto como salida. 
	GPIO = 0x00; // Se inicia el programa con el valor de cada salida en 0. 
 
    int sample = 100; //Elegida tras prueba y error. 
	int p = 47; // 47/4 = 11 y sobran 3.
    int q = 59; // 59/4 = 14 y sobran 3.
    int seed = 5; // Es un valor coprimo de M (M = p*q).
	
	unsigned int rand_num; 
	unsigned int temp = 0; 

    // Ciclo infinito 
    while (1)
    {	
		/* 
		Si se presiona el botón de reset, se genera un número aleatorio y este 
		es utilizado para iluminar los LEDs, según sea el caso (su valor).  
		*/
		if(GP3 == 1){	
			// Se genera un número aleatorio de tres bits. 
			rand_num = blum_blum_shub(p, q, seed); 

			// Verificación del caso para iluminar su resultado en los LEDs. 
			switch (rand_num) {
				case 0b001:
					GPIO = 0b00000001;
					delay(sample);
					break;

				case 0b010:
					GPIO = 0b00000010;
					delay(sample);
					break;

				case 0b011:
					GPIO = 0b00000011;
					delay(sample);
					break;

				case 0b100:
					GPIO = 0b00000110;
					delay(sample);
					break;

				case 0b101:
					GPIO = 0b00000111;
					delay(sample);
					break;

				case 0b110:
					GPIO = 0b00100110;
					delay(sample);
					break;

				default:
					break;
			}

			/*
			Se almacena el valor de GPIO actual para usarlo para aumentar la
			variabilidad en la generación de la semilla. 
			*/
			temp = GPIO;

			// Se apagan los LEDs para dar paso a la siguiente generación. 
			GPIO = 0x00;
		}

		// Se varía el valor de la semilla según sea el caso. 
		if ((seed % 7) == 0)
			seed += temp; 

		else
			seed += 7;
    }
}

/** @brief Función que simula un retraso de ejecución. 
 *  @details Para hacer que ocurra un retraso en la ejecución del programa,
 *  esta función utiliza un ciclo for anidado en otro utilizando en el primero 
 *  un número de ciclos aleatorio y en el segundo un sample definido por el 
 *  usuario.  
 *  @param sample valor de tipo entero.
*/
void delay(unsigned int sample)
{
	unsigned int i;
	unsigned int j;

	for(i=0;i<sample;i++)
	  for(j=0;j<1275;j++);
}

/** @brief Función que verifica si dos números son coprimos. 
 *  @details Esta función verifica si dos números son coprimos mediante la 
 *  revisón de que estos no tienen ningún factor primo en común, es decir, que 
 *  no tienen otro divisor común que 1. Lo anterior se logra mediante el 
 *  cálculo del máximo común divisor utilizando el algoritmo de Euclides. 
 *  @param a primer valor de tipo entero.
 *  @param b segundo valor de tipo entero.
 *  @return 1 si "a" es igual a 1 (el MCD entre "a" y "b" es 1).
*/
unsigned int sonCoprimos(int a, int b) {
    while (b != 0) {
        int temp = b;
        b = a % b;
        a = temp;
    }

    return a == 1;
}

/** @brief Función que verifica que la semilla ingresada es adecuada. 
 *  @details Para hacer esta verificación, se revisa que M y la semilla deben 
 *  ser coprimos, de lo contrario se debe modificar el valor de la semilla. 
 *  @param M número entero.
 *  @param seed número entero que representa la raíz.
 *  @return el valor de la semilla corregida. 
*/
int seed_adecuada(int M, int seed) {
    int seed_nueva = seed; 
    int aux = 1;

    while (aux) {
		if (sonCoprimos(seed_nueva, M) == 1) 
            aux = 0;
		
		else 
            seed_nueva += 7;
    }

    return seed_nueva;
}

/** @brief Función que calcula un número pseudo-aleatorio utilizando el
 *  algoritmo Blum-Blum-Shub. 
 *  @details Este algoritmo tiene la forma X(n+1)= (X(n)^2) % M, donde 
 *  @param p número entero y primo congruente con 3(mod 4).
 *  @param q número entero y primo congruente con 3(mod 4).
 *  @param seed número entero que representa la raíz.
 *  @return un número pseudoaleatorio entero sin signo.  
*/
unsigned int blum_blum_shub(int p, int q, int seed) {
	unsigned int random_num = 0;
	int M = p * q;
	int new_seed; 
	int x_n_plus_1; 
	int x_n; 
	int i; 

	// Se hacen las verificaciones necesarias de la semilla.
	new_seed = seed_adecuada(M, seed);

	x_n = new_seed; // se define x_0 = semilla verificada

	/* 
	Se realiza sólo 3 veces, ya que sólo son necesarios 3 bits para obtener 
	las 6 combinaciones posibles de un dado. 
	*/
	for (i = 0; i < 3; i++) {
        x_n_plus_1 = (x_n * x_n) % M; // Cálculo del algoritmo. 
        random_num |= (x_n_plus_1 & 1) << i; // Agrega el bit menos significativo de x_n1 a random_num.
        x_n = x_n_plus_1; // Actualiza x_n para la próxima iteración.
    }

    return random_num;
}